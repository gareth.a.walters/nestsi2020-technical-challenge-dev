Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      get '/tascas', to: 'tascas#index'
      post '/tascas', to: 'tascas#create'
      get '/tascas/:id', to: 'tascas#show'
      patch '/tascas/:id', to: 'tascas#update'
      delete '/tascas/:id', to: 'tascas#destroy'
    end
  end
end
