class CreateTascas < ActiveRecord::Migration[6.1]
  def change
    create_table :tascas do |t|
      t.string :name
      t.string :address
      t.integer :rating
      t.string :image_url

      t.timestamps
    end
  end
end
