class TascaSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :address, :rating, :image_url
end
