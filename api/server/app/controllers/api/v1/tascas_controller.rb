module Api::V1
  class TascasController < ApplicationController

    def index 
      @tascas = Tasca.all
      render json: TascaSerializer.new(@tascas).serialized_json
    end

    def show
      @tasca = Tasca.find(params[:id])
      render json: TascaSerializer.new(@tasca).serialized_json
    end

    def create
      @tasca = Tasca.new(tasca_params)

      if @tasca.save
        render json: TascaSerializer.new(@tasca).serialized_json
      else
        render json: { error: tasca.errors.messages }, status: 422
      end
    end

    def update
      @tasca = Tasca.find(params[:id])

      if @tasca.update(tasca_params)
        render json: TascaSerializer.new(@tasca).serialized_json
      else
        render json: { error: tasca.errors.messages }, status: 422
      end
    end

    def destroy
      @tasca = Tasca.find(params[:id])
      @tasca.destroy
    end

    private

    def tasca_params
      params.require(:tasca).permit(:name, :address, :rating)
    end

  end
end