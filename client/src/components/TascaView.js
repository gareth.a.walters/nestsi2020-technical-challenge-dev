import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Image from 'react-bootstrap/Image';
import Row from 'react-bootstrap/Row';
import { Link } from 'react-router-dom';
import TascaMap from './Map/TascaMap';

const TascaView = (props) => {
  const [tasca, setTasca] = useState();
  const id = props.match.params.id;
  const url = `/api/v1/tascas/${id}`;

  useEffect(() => {
    showTasca();
  }, []);

  //show single tasca - GET
  const showTasca = async () => {
    try {
      const res = await axios.get(url);
      setTasca(res.data.data.attributes);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      <Container className='mt-4'>
        <Row>
          <Col>
            <Row>
              <Link to={'/'}>
                <Button className='ml-3 mb-3' variant='outline-primary'>
                  Back to tascas
                </Button>
              </Link>
            </Row>
            <Row>
              <Col xs={12} md={8}>
                <Image src={tasca?.image_url} thumbnail />
              </Col>
            </Row>
            <Row className='mt-4'>
              <Col xs={12} md={8}>
                <h1>{tasca?.name}</h1>
              </Col>
            </Row>
            <Row className='mt-4'>
              <Col xs={12} md={8}>
                <h3>Address</h3>
              </Col>
            </Row>
            <Row>
              <Col xs={12} md={8}>
                <p>{tasca?.address}</p>
              </Col>
            </Row>
            <Row className='mt-4'>
              <Col xs={12} md={8}>
                <h3>Rating</h3>
              </Col>
            </Row>
            <Row>
              <Col xs={12} md={8}>
                <p>{tasca?.rating}</p>
              </Col>
            </Row>
          </Col>
          <Col>
            <p>Click the address below to view location:</p>
            <TascaMap address={tasca?.address} />
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default TascaView;
