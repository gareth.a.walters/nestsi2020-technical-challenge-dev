import 'mapbox-gl/dist/mapbox-gl.css';
import React, { useCallback, useRef, useState } from 'react';
import MapGL from 'react-map-gl';
import Geocoder from 'react-map-gl-geocoder';
import 'react-map-gl-geocoder/dist/mapbox-gl-geocoder.css';

const MAPBOX_TOKEN =
  'pk.eyJ1IjoiZ2FyZXRodyIsImEiOiJja3BpZDdzeWUwOG42MnJwbm0ycXE3cTRhIn0.xJFdKsfPr0I8IXJBdpG9fw';

const TascaMap = ({ address }) => {
  const [viewport, setViewport] = useState({
    latitude: 40.20564,
    longitude: -8.42965,
    zoom: 12,
  });
  const mapRef = useRef();

  //handle user viewport changes
  const handleViewportChange = useCallback(
    (newViewport) => setViewport(newViewport),
    []
  );

  //handler geocoder wrapper viewport changes
  const handleGeocoderViewportChange = useCallback((newViewport) => {
    const geocoderDefaultOverrides = { transitionDuration: 1000 };

    return handleViewportChange({
      ...newViewport,
      ...geocoderDefaultOverrides,
    });
  }, []);

  return (
    <div style={{ height: '100vh' }}>
      <MapGL
        ref={mapRef}
        {...viewport}
        width='100%'
        height='50%'
        onViewportChange={handleViewportChange}
        mapStyle='mapbox://styles/garethw/ckpidopeg19ua17p41ndoqu9o'
        mapboxApiAccessToken={MAPBOX_TOKEN}
      >
        <Geocoder
          mapRef={mapRef}
          onViewportChange={handleGeocoderViewportChange}
          mapboxApiAccessToken={MAPBOX_TOKEN}
          position='top-left'
          inputValue={`${address} Coimbra`}
          limit={1}
          zoom={16}
        />
      </MapGL>
    </div>
  );
};

export default TascaMap;
