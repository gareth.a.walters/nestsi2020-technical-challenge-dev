import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import TascaEdit from './TascaEdit';
import Tascas from './Tascas/Tascas';
import TascaView from './TascaView';

const App = () => {
  return (
    <Switch>
      <Route exact path='/' render={(props) => <Tascas {...props} />} />
      <Route
        exact
        path='/tascas/view/:id'
        render={(props) => <TascaView {...props} />}
      />
      <Route
        exact
        path='/tascas/edit/:id'
        render={(props) => <TascaEdit {...props} />}
      />
    </Switch>
  );
};

export default App;
