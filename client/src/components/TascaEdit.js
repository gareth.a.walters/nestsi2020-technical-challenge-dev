import axios from 'axios';
import React, { useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import { Link } from 'react-router-dom';

const TascaEdit = (props) => {
  const [tasca, setTasca] = useState({});
  const id = props.match.params.id;
  const url = `/api/v1/tascas/${id}`;

  useEffect(() => {
    showTasca();
  }, []);

  //show single tasca - GET
  const showTasca = async () => {
    try {
      const res = await axios.get(url);
      setTasca(res.data.data.attributes);
    } catch (err) {
      console.log(err);
    }
  };

  //edit tasca - PATCH
  const editTasca = async (updatedTasca) => {
    try {
      await axios.patch(url, {
        name: updatedTasca.name,
        address: updatedTasca.address,
        rating: updatedTasca.rating,
      });
    } catch (err) {
      console.log(err);
    }
  };

  //handle user input change
  const handleChange = (e) => {
    e.preventDefault();
    setTasca(Object.assign({}, tasca, { [e.target.id]: e.target.value }));
  };

  return (
    <>
      <Container className='border rounded mt-4'>
        <form className='p-3' onSubmit={() => editTasca(tasca)}>
          <div className='form-group'>
            <label htmlFor='name'>Name</label>
            <input
              type='text'
              className='form-control'
              id='name'
              placeholder='Tasca name'
              value={tasca?.name}
              onChange={handleChange}
            ></input>
          </div>
          <div className='form-group'>
            <label htmlFor='address'>Address</label>
            <input
              type='text'
              className='form-control'
              id='address'
              placeholder='Tasca address'
              value={tasca?.address}
              onChange={handleChange}
            ></input>
          </div>
          <div className='form-group'>
            <label htmlFor='rating'>Rating (1-10)</label>
            <select
              className='form-control'
              id='rating'
              value={tasca?.rating}
              onChange={handleChange}
            >
              <option value='1'>1</option>
              <option value='2'>2</option>
              <option value='3'>3</option>
              <option value='4'>4</option>
              <option value='5'>5</option>
              <option value='6'>6</option>
              <option value='7'>7</option>
              <option value='8'>8</option>
              <option value='9'>9</option>
              <option value='10'>10</option>
            </select>
          </div>
          <div>
            <Button
              variant='success'
              type='submit'
              onClick={() => {
                editTasca(tasca);
              }}
            >
              Update
            </Button>
            <Link to={'/'}>
              <Button className='ml-3' variant='outline-secondary'>
                Cancel
              </Button>
            </Link>
          </div>
        </form>
      </Container>
    </>
  );
};

export default TascaEdit;
