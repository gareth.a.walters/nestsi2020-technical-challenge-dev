import React from 'react';
import Form from 'react-bootstrap/Form';

const SearchBar = ({ setSearch }) => {
  return (
    <Form.Control
      type='text'
      placeholder='Search tascas by name'
      onChange={(event) => {
        setSearch(event.target.value);
      }}
    />
  );
};

export default SearchBar;
