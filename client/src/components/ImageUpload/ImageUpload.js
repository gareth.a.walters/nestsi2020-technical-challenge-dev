import React, { useRef } from 'react';
import placeholderIcon from '../../assets/icons/placeholder_icon.svg';
import styles from './ImageUpload.module.css';

const ImageUpload = ({ image, setImage, id }) => {
  const hiddenFileInput = useRef(null);

  //add image click - shows file menu
  const addImageClick = () => {
    hiddenFileInput.current.click();
  };

  //remove image click - removes selected image
  const removeImageClick = () => {
    setImage('');
  };

  //file menu input change handler
  const handleImageChange = ({ target: { files } }) => {
    const image = URL.createObjectURL(files[0]);
    setImage(image);
    hiddenFileInput.current.value = null;
  };

  return (
    <>
      <div className={styles.container}>
        {!image ? (
          <button
            type='button'
            className={styles.imageUploadButton}
            onClick={addImageClick}
          >
            <img src={placeholderIcon} alt='Add photos button' />
            <p className={styles.imageUploadText}>Click to add image</p>
          </button>
        ) : (
          <div className={styles.imageWrapper}>
            <img src={image} className={styles.previewImage} alt='Selected' />
            <button
              className={styles.removeImageBtn}
              onClick={() => {
                removeImageClick(image);
              }}
            ></button>
          </div>
        )}
        <input
          type='file'
          accept='image/*'
          ref={hiddenFileInput}
          onChange={handleImageChange}
          className={styles.file}
          multiple
        />
      </div>
    </>
  );
};

export default ImageUpload;
