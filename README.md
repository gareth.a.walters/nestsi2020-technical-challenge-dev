# NEST Summer Internship Challenge 2021

**Requirements**

- Ruby 3.0.1
- Rails 6.1.3.2
- PostgreSQL 13.3

**Project Setup**

**Frontend**

`$ cd client`

`$ npm i`

`$ npm start`


**Backend**

`$ cd api/server`

`$ bundle install`

`$ rails db:setup`

`$ rails s -p 3001`
